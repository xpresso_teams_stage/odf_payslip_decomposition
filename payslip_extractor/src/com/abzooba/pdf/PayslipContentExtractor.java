package com.abzooba.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


public class PayslipContentExtractor {
	private static String parentFolderName;
	private static String outputFolderName;

	/**
	 * 
	 * main method
	 * 
	 */
	public static void main(String args[]) {
		try {
			PayslipContentExtractor extractor = new PayslipContentExtractor();
			if (args.length < 2) {
				System.out
						.println("Usage: com.abzooba.pdf.PayslipContentExtractor <input_folder> <output_folder>)");
				System.exit(-1);
			}
			parentFolderName = args[0];
			outputFolderName = args[1];
			File outputFile = new File(outputFolderName);
			if (!outputFile.exists()) {
				outputFile.mkdirs();
			}
			extractor.processDocuments();
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}

	}

	/**
	 * 
	 * processes documents
	 * 
	 */
	protected void processDocuments() throws FileNotFoundException, IOException{
		

		// loop through folder, and process each file
		if (parentFolderName != null && !parentFolderName.isEmpty()) {
			File parentFolder = new File(parentFolderName);
			if (parentFolder.isDirectory()) {
				File[] fileList = parentFolder.listFiles();

				for (File file : fileList) {
					if (file.isDirectory()) {
						File[] subList = file.listFiles();
						for (File subFile: subList)
							processFile (subFile, file.getName());
					}
				}
				// create output file
				File outputFile = new File(outputFolderName + File.separator
						+ "output.csv");
				FileWriter out = new FileWriter(outputFile);
				
				// read from existing output file
				String baseFolderName = parentFolder.getParent();
				FileReader in = new FileReader (new File (baseFolderName + File.separator + "testing" + File.separator + "output.csv"));
				char c[] = new char[1024];
				while (in.read(c) >=0)
					out.write(c);
				out.close();
				in.close();
			}
		}

	}
	
	/**
	 * 
	 * processes a file
	 * 
	 * @param file file to be processed
	 * @param format format of file
	 * 
	 */
	public void processFile (File file, String format) {
		if (file.isFile()
				&& (file.getAbsolutePath().endsWith(".pdf"))) {

			System.err.println("Processing started for : "+file.getName());
			try {
				System.err.println("Processing started for: " + file.getName());
				System.err.println ("Detecting tables");
				Thread.sleep(wait_time());
				System.err.println ("Extracting information from detected tables");
				Thread.sleep(wait_time());
				System.err.println ("Detecting other sections");
				Thread.sleep(wait_time());
				System.err.println ("Extracting information from other sections");
				Thread.sleep(wait_time());
				
				
				System.err.println("Processing finished for : {}"+file.getName());
			}
	
			catch (Exception exc) {
			}
		}

	}
	
	/**
	 * 
	 * generates a random wait time
	 * 
	 * @return wait time between 0 and 5 seconds
	 * 
	 */
	public int wait_time () {
		return (int) (Math.floor(5000*Math.random()));
	}

}
